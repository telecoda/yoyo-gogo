#!/bin/bash
go test ./$1 -coverprofile=/tmp/coverage.out
go tool cover -html=/tmp/coverage.out
rm /tmp/coverage.out