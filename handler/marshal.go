package handler

import "bitbucket.org/telecoda/yoyo-gogo/domain"

// convert domain object from & to handler (API) objects

// MARSHAL_START OMIT

func (c *Customer) fromDomain(domainCustomer domain.Customer) {
	c.Id = domainCustomer.Id
	c.FirstName = domainCustomer.Forename
	c.LastName = domainCustomer.Surname
}

func (c Customer) toDomain() domain.Customer {

	return domain.Customer{
		Id:       c.Id,
		Forename: c.FirstName,
		Surname:  c.LastName,
	}
}

// MARSHAL_END OMIT
