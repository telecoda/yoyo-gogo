package handler

import (
	"encoding/json"
	"net/http"
)

// JSON_RESPONSE_START OMIT
func JSONResponse(w http.ResponseWriter, obj interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(obj); err != nil {
		panic(err)
	}

}

// JSON_RESPONSE_END OMIT
