package handler

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"

	"bitbucket.org/telecoda/yoyo-gogo/domain"
)

// CUSTOMERS_START OMIT
func CustomersHandler(w http.ResponseWriter, req *http.Request) {

	startTime := time.Now()
	log.Printf("Request received")

	switch req.Method {
	case "GET":
		// get customers
		customers, err := readAll()
		if err != nil {
			http.Error(w, err.Error(), 500) // internal error
			return
		}

		log.Printf("Request response: Duration:%s", time.Since(startTime))

		JSONResponse(w, customers)

	default:
		http.Error(w, "GET requests only supported on this endpoint", 400) // bad request
	}

}

// CUSTOMERS_END OMIT

func CustomerHandler(w http.ResponseWriter, req *http.Request) {

	startTime := time.Now()
	log.Printf("Request received")

	vars := mux.Vars(req)
	id := vars["id"]

	switch req.Method {
	case "GET":
		// get customers
		customer, err := readById(id)
		if err != nil {
			http.Error(w, err.Error(), 500) // internal error
		}

		if customer == nil {
			http.Error(w, fmt.Sprintf("Customer:%s not found", id), 404)
			return
		}

		log.Printf("Request response: Duration:%s", time.Since(startTime))
		JSONResponse(w, customer)

	default:
		http.Error(w, "GET requests only supported on this endpoint", 400) // bad request

	}

}

// READALL_START OMIT
func readAll() ([]Customer, error) {
	domainCustomers, err := domain.ReadAllCustomers()
	if err != nil {
		return nil, err
	}

	// convert domain objects to API objects
	apiCustomers := make([]Customer, 0)
	for _, domainCustomer := range domainCustomers {
		apiCustomer := Customer{}
		apiCustomer.fromDomain(domainCustomer)
		apiCustomers = append(apiCustomers, apiCustomer)
	}

	return apiCustomers, nil

}

// READALL_END OMIT

func readById(id string) (*Customer, error) {
	domainCustomer, err := domain.ReadCustomerById(id)
	if err != nil {
		return nil, err
	}

	if domainCustomer == nil {
		return nil, nil
	}

	// convert domain objects to API objects
	apiCustomer := Customer{}
	apiCustomer.fromDomain(*domainCustomer)

	return &apiCustomer, nil

}
