// INTEGRATION_SETUP_START OMIT
// +build integration
package handler

// INTEGRATION_SETUP_END OMIT

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/telecoda/yoyo-gogo/dao"
	"bitbucket.org/telecoda/yoyo-gogo/domain"

	"github.com/stretchr/testify/assert"
)

// TEST_SETUP_START OMIT
func SetupPostgres() {
	dao, err := dao.NewPostgresDAO()
	if err != nil {
		errorMsg := fmt.Sprintf("Failed to setup Postgres DAO:%s", err)
		panic(errorMsg)
	}
	domain.DefaultCustomerDAO = dao
}

// TEST_SETUP_END OMIT

func TestAPIReadAll(t *testing.T) {

	SetupPostgres()

	req, err := http.NewRequest("GET", "http://example.com/api/customers", nil)
	if err != nil {
		log.Fatal(err)
	}

	w := httptest.NewRecorder()

	// call handler
	CustomersHandler(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "Check header code")
	assert.NotNil(t, w.Body, "Expect a response in the body")
	assert.Equal(t, "application/json; charset=UTF-8", w.HeaderMap["Content-Type"][0], "Expect a json encoded response")

	customers := make([]Customer, 0)

	json.Unmarshal(w.Body.Bytes(), &customers)

	assert.Equal(t, 5, len(customers), "Expect 5 customers")
}

// TEST_BYID_START OMIT
func TestAPIReadById(t *testing.T) {
	SetupPostgres()
	req, err := http.NewRequest("GET", "http://example.com/api/customers/21102015", nil)
	if err != nil {
		log.Fatal(err)
	}
	w := httptest.NewRecorder()

	// call handler
	CustomerHandler(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "Check header code")
	assert.NotNil(t, w.Body, "Expect a response in the body")
	assert.Equal(t, "application/json; charset=UTF-8", w.HeaderMap["Content-Type"][0], "Expect a json encoded response")

	customer := Customer{}
	json.Unmarshal(w.Body.Bytes(), &customer)
	expectedCustomer := Customer{
		Id:        "21102015",
		FirstName: "Marty",
		LastName:  "McFly",
	}
	assert.Equal(t, expectedCustomer, customer, "Check customers are the same")
}

// TEST_BYID_END OMIT
