package handler

// HANDLER_TYPES_START OMIT
// Type defs for API layer objects

type Customer struct {
	Id        string
	FirstName string
	LastName  string
}

// HANDLER_TYPES_END OMIT

// Example of API request/responses not currently used

type CreateCustomerReq struct {
	customer Customer
}

type CreateCustomerResp struct {
}

type ReadCustomerReq struct {
	id string
}

type ReadCustomerResp struct {
	customer Customer
}

type ReadCustomersReq struct {
}

type ReadCustomersResp struct {
	customers []Customer
}

type UpdateCustomerReq struct {
	customer Customer
}

type UpdateCustomerResp struct {
}

type DeleteCustomerReq struct {
	id string
}

type DeleteCustomerResp struct {
}

type APIHandler struct {
}

type CustomerAPI interface {
	Create(req CreateCustomerReq) (*CreateCustomerResp, error)
	Read(req ReadCustomerReq) (*ReadCustomerResp, error)
	ReadAll(req ReadCustomersReq) (*ReadCustomersResp, error)
	Update(req UpdateCustomerReq) (*UpdateCustomerResp, error)
	Delete(req DeleteCustomerReq) (*UpdateCustomerResp, error)
}
