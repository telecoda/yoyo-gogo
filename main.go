package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"bitbucket.org/telecoda/yoyo-gogo/dao"
	"bitbucket.org/telecoda/yoyo-gogo/domain"
	"bitbucket.org/telecoda/yoyo-gogo/handler"
)

func main() {

	initDependencies()

	// HTTP Code
	// init routing
	r := mux.NewRouter()
	r.HandleFunc("/api/customers", handler.CustomersHandler)
	r.HandleFunc("/api/customers/{id}", handler.CustomerHandler)
	http.Handle("/", r)

	log.Println("Listening on port 12345")
	http.ListenAndServe(":12345", nil)
}

func initDependencies() {
	dao, err := dao.NewPostgresDAO()

	if err != nil {
		panic(fmt.Sprintf("Error initialising DAO:%s", err))
	}

	domain.DefaultCustomerDAO = dao

}
