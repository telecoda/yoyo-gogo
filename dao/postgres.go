package dao

import (
	"database/sql"
	"fmt"
	"log"

	domain "bitbucket.org/telecoda/yoyo-gogo/domain"

	_ "github.com/lib/pq"
)

//////////////////////////////////////
// PostgresDAO                      //
//////////////////////////////////////

// POSTGRES_DAO_START OMIT
type PostgresDAO struct {
	db *sql.DB
}

func NewPostgresDAO() (*PostgresDAO, error) {
	dao := PostgresDAO{}
	db, err := dao.connect()
	if err != nil {
		return nil, err
	}
	// save db handle
	dao.db = db
	return &dao, nil // return a pointer
}

// POSTGRES_DAO_END OMIT

func (p *PostgresDAO) connect() (*sql.DB, error) {
	boot2dockerHost := "192.168.59.104"
	user := "postgres"
	password := "password"
	port := "5432"
	dbName := "yoyodemo"
	sslMode := "disable"
	connectionUrl := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s", user, password, boot2dockerHost, port, dbName, sslMode)

	db, err := sql.Open("postgres", connectionUrl)
	if err != nil {
		log.Fatalf("Failed to connect to db:%s", err)
		return nil, err
	}

	return db, nil

}

// POSTGRES_CREATE_START OMIT
// Create a customer
func (p *PostgresDAO) Create(customer domain.Customer) error {

	_, err := p.db.Exec("INSERT into customer values($1,$2,$3)", customer.Id, customer.Forename, customer.Surname)

	return err // error or nil
}

// POSTGRES_CREATE_END OMIT

// Read a customer
func (p PostgresDAO) Read(id string) ([]domain.Customer, error) {

	var rows *sql.Rows
	var err error

	if id == "" {
		// read by id, append where clause
		rows, err = p.db.Query("SELECT id, forename, surname FROM customer")
	} else {
		// read by id
		rows, err = p.db.Query("SELECT id, forename, surname FROM customer where id = $1", id)
	}
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close() // close cursor at end

	return readCustomers(rows)

}

func readCustomers(rows *sql.Rows) ([]domain.Customer, error) {

	results := make([]domain.Customer, 0) // create empty array

	for rows.Next() {
		customer := domain.Customer{}
		if err := rows.Scan(&customer.Id, &customer.Forename, &customer.Surname); err != nil {
			log.Fatal(err)
			return results, err
		}
		results = append(results, customer) // append to array
	}

	return results, nil
}

// Search for customers by forename
func (p *PostgresDAO) SearchByForename(forename string) ([]domain.Customer, error) {

	rows, err := p.db.Query("SELECT id, forename, surname FROM customer WHERE forename = $1", forename)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close() // close cursor at end

	return readCustomers(rows)
}

// Update a customer
func (p *PostgresDAO) Update(customer domain.Customer) error {

	_, err := p.db.Exec("UPDATE customer set forename = $1, surname = $2 where id = $3", customer.Forename, customer.Surname, customer.Id)

	return err // error or nil

}

// Delete a customer
func (p *PostgresDAO) Delete(id string) error {

	_, err := p.db.Exec("DELETE from customer where id = $1", id)

	return err // error or nil

}

// Close db handle
func (p *PostgresDAO) Close() {

	if p.db != nil {
		log.Println("Closing Postgres db handle")
		p.db.Close()
	}

}
