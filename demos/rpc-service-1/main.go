package main

import (
	"fmt"

	"bitbucket.org/telecoda/yoyo-gogo/dao"
	"bitbucket.org/telecoda/yoyo-gogo/demos/rpc-service-1/handler"
	"bitbucket.org/telecoda/yoyo-gogo/domain"

	service "bitbucket.org/telecoda/library-microservices/server"
)

func main() {

	service.Name = "com.yoyo.service.rpc-service-1"
	service.Description = "Demo RPC service"
	service.Version = ServiceVersion
	service.Group = "Demo"
	service.Source = "bitbucket.org/telecoda/yoyo-gogo"
	service.OwnerEmail = "rob@justyoyo.com"
	service.OwnerMobile = "+xxxx"
	service.OwnerTeam = "platform"

	initDependencies()

	// register endpoints
	service.RegisterEndpoint(&service.Endpoint{
		Name:    "create",
		Handler: handler.RPCCreate,
	})

	service.Init()
	service.Run()
}

func initDependencies() {
	dao, err := dao.NewPostgresDAO()

	if err != nil {
		panic(fmt.Sprintf("Error initialising DAO:%s", err))
	}

	domain.DefaultCustomerDAO = dao

}
