package handler

import (
	"fmt"

	"code.google.com/p/goprotobuf/proto"
	log "github.com/cihub/seelog"

	"bitbucket.org/telecoda/library-microservices/errors"
	"bitbucket.org/telecoda/library-microservices/server"

	pcreate "bitbucket.org/telecoda/yoyo-gogo/proto/create"
)

func RPCCreate(req *server.Request) (proto.Message, errors.Error) {

	log.Infof("Handler Request received:%#v", req)

	errorCode := server.Name + ".create"

	request := pcreate.Request{}

	err := req.Unmarshal(&request)

	if err != nil {
		return nil, errors.BadRequest(errorCode, fmt.Sprintf("Error unmarshaling customer data: %s", err))
	}

	log.Infof("Proto request Customer:%#s", request.GetCustomer())

	// startTime := time.Now()
	// log.Printf("Request received")

	// switch req.Method {
	// case "GET":
	// 	// get customers
	// 	customers, err := readAll()
	// 	if err != nil {
	// 		http.Error(w, err.Error(), 500) // internal error
	// 		return
	// 	}

	// 	log.Printf("Request response: Duration:%s", time.Since(startTime))

	// 	JSONResponse(w, customers)

	// default:
	// 	http.Error(w, "GET requests only supported on this endpoint", 400) // bad request
	// }

	return nil, nil

}
