package main

import (
	"fmt"
	"log"

	"bitbucket.org/telecoda/yoyo-gogo/dao"
	"bitbucket.org/telecoda/yoyo-gogo/domain"

	_ "github.com/lib/pq"
)

func createTestCustomers(dao domain.CustomerDAO) error {

	customers := []domain.Customer{
		domain.Customer{Id: "21102015", Forename: "Marty", Surname: "McFly"},
		domain.Customer{Id: "04051977", Forename: "Luke", Surname: "Skywalker"},
		domain.Customer{Id: "999", Forename: "PC", Surname: "Plod"},
		domain.Customer{Id: "31032015", Forename: "Keith", Surname: "Flint"},
		domain.Customer{Id: "696969", Forename: "Keith", Surname: "Lemon"},
	}
	// create some customers
	for _, customer := range customers {
		err := dao.Create(customer)
		if err != nil {
			return fmt.Errorf("Error creating test customers:%s", err)

		}
	}

	return nil
}

func deleteAllTestCustomers(d domain.CustomerDAO) error {

	// read all
	customers, err := d.Read("")
	if err != nil {
		return err
	}

	// delete each customers
	for _, customer := range customers {
		err := d.Delete(customer.Id)
		if err != nil {
			return fmt.Errorf("Error deleting test customers:%s", err)

		}
	}

	return nil
}

func setupTestCustomers(d domain.CustomerDAO) error {
	err := deleteAllTestCustomers(d)
	if err != nil {
		return err
	}

	err = createTestCustomers(d)
	if err != nil {
		return err
	}

	return nil
}

func main() {

	// POSTGRES_DEMO_START OMIT
	d, err := dao.NewPostgresDAO()
	if err != nil {
		log.Fatalf("Failed to create dao instance:%s", err)
		return
	}
	defer d.Close() // close at end

	fmt.Printf("Setting up customers\n")
	err = setupTestCustomers(d)
	if err != nil {
		log.Fatalf("There was an error setting up the test customers:%s", err)
		return
	}

	customers, err := d.SearchByForename("Keith")
	fmt.Println("Results:")
	for _, customer := range customers {
		fmt.Printf("Customer:%#v\n", customer)
	}
	// POSTGRES_DEMO_END OMIT
}
