package domain

var (
	DefaultCustomerDAO CustomerDAO
)

// Type defs for Domain layer objects

// DOMAIN_TYPES_START OMIT
type Customer struct {
	Id       string
	Forename string
	Surname  string
}

// DOMAIN_TYPES_END OMIT

// INTERFACE_START OMIT
type CustomerDAO interface {
	Create(customer Customer) error
	Read(id string) ([]Customer, error)
	SearchByForename(forename string) ([]Customer, error)
	Update(customer Customer) error
	Delete(id string) error
	Close()
}

// INTERFACE_END OMIT
