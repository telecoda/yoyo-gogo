package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// TEST_SETUP_START OMIT
func SetupInMemory() {
	DefaultCustomerDAO = NewInMemoryDAO()
	createTestCustomers()
}

// TEST_SETUP_END OMIT

func TestReadAll(t *testing.T) {
	SetupInMemory()

	customers, err := ReadAllCustomers()
	assert.Nil(t, err, "Error not expected")
	assert.Equal(t, 5, len(customers), "Check result count")

}

// TEST_BYID_START OMIT
func TestReadById(t *testing.T) {
	SetupInMemory()

	customer, err := ReadCustomerById("21102015")

	assert.Nil(t, err, "Error not expected")
	assert.NotNil(t, customer, "Customer expected")

	expectedCustomer := &Customer{
		Id:       "21102015",
		Forename: "Marty",
		Surname:  "McFly",
	}

	assert.Equal(t, expectedCustomer, customer, "Check customers are the same")
}

// TEST_BYID_END OMIT
