package domain

// DOMAIN_LOGIC_START OMIT
func ReadAllCustomers() ([]Customer, error) {

	return DefaultCustomerDAO.Read("")

}

func ReadCustomerById(id string) (*Customer, error) {

	customers, err := DefaultCustomerDAO.Read(id)
	if err != nil {
		return nil, err
	}

	if len(customers) == 0 {
		return nil, nil
	}

	return &customers[0], nil
}
// DOMAIN_LOGIC_END OMIT
