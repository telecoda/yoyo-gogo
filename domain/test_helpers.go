package domain

import (
	"fmt"
	"log"
)

func createTestCustomers() error {
	customers := []Customer{
		Customer{Id: "21102015", Forename: "Marty", Surname: "McFly"},
		Customer{Id: "04051977", Forename: "Luke", Surname: "Skywalker"},
		Customer{Id: "999", Forename: "PC", Surname: "Plod"},
		Customer{Id: "31032015", Forename: "Keith", Surname: "Flint"},
		Customer{Id: "696969", Forename: "Keith", Surname: "Lemon"},
	}
	// create some customers
	for _, customer := range customers {
		err := DefaultCustomerDAO.Create(customer)
		if err != nil {
			return fmt.Errorf("Error creating test customers:%s", err)
		}
	}

	return nil
}

//////////////////////////////////////
// InMemoryDAO                      //
//////////////////////////////////////

// INMEMORY_DAO_START OMIT
type InMemoryDAO struct {
	customers map[string]Customer
}

func NewInMemoryDAO() *InMemoryDAO {

	customers := make(map[string]Customer)

	dao := InMemoryDAO{
		customers: customers,
	}

	return &dao // return a pointer
}

// INMEMORY_DAO_END OMIT

// INMEMORY_CREATE_START OMIT
// Create a customer
func (m *InMemoryDAO) Create(customer Customer) error {

	id := customer.Id

	m.customers[id] = customer

	return nil
}

// INMEMORY_CREATE_END OMIT

// Read a customer
func (m *InMemoryDAO) Read(id string) ([]Customer, error) {

	results := make([]Customer, 0) // create empty array

	if id == "" {
		// return all
		for _, customer := range m.customers {
			results = append(results, customer)
		}

		return results, nil
	}

	if customer, ok := m.customers[id]; ok {
		results = append(results, customer)
		return results, nil
	}
	return nil, nil

}

// Search for customers by forename
func (m *InMemoryDAO) SearchByForename(forename string) ([]Customer, error) {

	results := make([]Customer, 0) // create empty array

	for _, customer := range m.customers {
		if customer.Forename == forename {
			results = append(results, customer)
		}
	}

	return results, nil
}

// Update a customer
func (m *InMemoryDAO) Update(customer Customer) error {

	id := customer.Id

	m.customers[id] = customer

	return nil

}

// Delete a customer
func (m *InMemoryDAO) Delete(id string) error {

	// lookup and delete if found
	if _, ok := m.customers[id]; ok {
		delete(m.customers, id)
		return nil
	} else {
		return fmt.Errorf("Cannot delete id:%s it does not exist", id)
	}

}

// Close
func (m *InMemoryDAO) Close() {

	// nothing to close
	log.Println("Closing in memory DAO")

}
